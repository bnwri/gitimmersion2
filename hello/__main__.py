# Author: Ben Wright, wazzup@tp.net, theinternetknows.us
# Date: 3/18/2020

import sys

# Default name is exactly 'World'.
# Defaults are really great!
name = sys.argv[1] if len(sys.argv) > 1 else 'World'
print("Hello, world!")